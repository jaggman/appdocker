#!/usr/bin/env bash

name=app
port=8888
args=
test=false
repair=false
app=true
db=false
sphinx=false
dump=false
marianame=mariad
sphinxname=sphinxd
dbname=
mariadatadir="$(dirname $(pwd))/mysql/data"
sphinxdatadir="$(dirname $(pwd))/sphinx/data"
sphinxlogsdir="$(dirname $(pwd))/sphinx/logs"
bridge=isolated_app
dumpdir=



help="Hello!\n
\n\n\nVerify that you are in the application folder!\n\n\n
"
help+="usage: \n\t$0 [args]\n"
help+="\nargs:\n
\t-n {container name}\tDefault $name\n
\t-p {exploded port}\tDefault $port\n
\t-l {container name} {alias}\tlink another container to App\n
\t-v {local volume} {container volume}\n
\t--sphinx {container name}\n
\t--maria {container name} {db name}\n
\t-d|--dump {dump_file.sql.gz}\n
\t-b|--bridge {network name}\tDefault $bridge\n
"
while [ $# -gt 0 ]
do
    case "$1" in
        -t|--test) test=true
            ;;
        -h|--help)
            echo -en $help
            exit 0
            ;;
        -n|--name) name="$2"
            shift
            ;;
        --maria) db=true ; marianame="$2" ; dbname="$3"
            shift; shift
            ;;
        --sphinx) sphinx=true ; sphinxname="$2"
            shift
            ;;
        -p|--port) port="$2"
            shift
            ;;
        -b|--bridge) bridge="$2"
            shift
            ;;
        -d|--dump) dump=true ; dumpdir="$(pwd)/$2"
            shift
            ;;
        --db)
            shift
            ;;
        -l|--link) args+="--link $2:$3 "
            shift; shift
            ;;
        -v|--volume) args+="-v $2:$3 "
            shift; shift
            ;;
        -r|--repair) repair=true
            ;;
        --no-app) app=false
            ;;
        *) echo "Unknown arg $1"
            exit 1
            ;;
    esac
    shift
done


if [ "$repair" = true ] ;
then
    echo -en 'Start repair...\n'
    docker stop $name && docker rm $name
    docker stop $sphinxname && docker rm $sphinxname
    docker stop $marianame && docker rm $marianame
    docker network rm $bridge
    exit 0
fi

echo -e '\n****TEST****\n'

docker network create --driver bridge $bridge

if [ "$db" = true ] ;
then
    ###########################
    ###  Creating database  ###
    ###########################

    if [ "$(docker inspect --format='{{.Name}}' $(docker ps -aq --no-trunc) | grep -e "^/$marianame\$")" = "/$marianame" ] ;
    then
        echo -e "Mariadb already created:\t starting $marianame \n" ;
        docker start $marianame ;
        echo -e "Connecting $marianame to $bridge\n" ;
        docker network connect --alias=mysql $bridge $marianame
    else
        echo -e "No mariadb ($marianame) found...\t creating:"
        mkdir -p "$mariadatadir"
        docker run \
        --network=$bridge \
        --network-alias=mysql \
        --restart=always \
        --name $marianame \
        -v $mariadatadir:/var/lib/mysql \
        -e MYSQL_ALLOW_EMPTY_PASSWORD=true \
        -d mariadb
    fi

    if [ "$dump" = true ] ;
        then
            echo 'Drop database...'
            while true
            do
                case `echo "drop database if exists $dbname;" | docker exec -i $marianame mysql 2>&1` in
                    ERROR\ 200*)
                       echo '.' ; sleep 1 ;;
                    *) echo -e 'Database created' ; break ;;
                esac
            done
            echo 'Create database...'
            echo "create database if not exists $dbname;" | docker exec -i $marianame mysql 2>&1
            echo 'Loading dump...'
            while true
            do
                case `zcat < $dumpdir | docker exec -i $marianame mysql $dbname 2>&1` in
                    ERROR\ 200*)
                       echo '.' ; sleep 1 ;;
                    *) echo -e 'Dump loaded' ; break ;;
                esac
            done
            echo 'dump loaded'
    fi

fi

if [ "$sphinx" = true ] ;
then

    ###########################
    ###   Creating sphinx   ###
    ###########################

    if [ "$(docker inspect --format='{{.Name}}' $(docker ps -aq --no-trunc) | grep -e "^/$sphinxname\$")" = "/$sphinxname" ] ;
    then
        echo -e "Sphinx already created:\t starting $sphinxname \n" ;
        docker start $sphinxname ;
        echo -e "Connecting $sphinxname to $bridge\n" ;
        docker network connect --alias=sphinx $bridge $sphinxname
    else
        echo -e "No Sphinx ($sphinxname) found...\t creating:"
        mkdir -p $sphinxdatadir
        mkdir -p $sphinxlogsdir
        docker run \
            --restart=always \
            --name $sphinxname \
            --network=$bridge \
            --network-alias=sphinx \
            -v $sphinxdatadir:/var/lib/sphinx/data \
            -v $sphinxlogsdir:/var/log/sphinx \
            -v $(pwd)/config/sphinx.conf:/etc/sphinxsearch/sphinx.conf \
            -d jaggman/sphinx
    fi

fi



if [ "$app" = true ] ;
then

    ###########################
    ###    Creating App     ###
    ###########################

    echo "Creating app..."
    docker run -d \
        --restart=always \
        --name $name \
        --network=$bridge \
        --network-alias=$name \
        $args \
        -v $(pwd)/:/var/www/html \
        -v $(pwd)/config/worker.conf:/etc/supervisor/conf.d/worker.conf:ro \
        -p $port:80 \
        jaggman/app

    docker exec -i $name bash <<EOF
        cd /var/www/html
        composer install
        exit
EOF

    if [ "$db" = true ] ;
    then
        docker exec -i $name /var/www/html/yii migrate
    fi

    if [ "$sphinx" = true ] ;
    then
        docker exec -i $sphinxname bash /index.sh
    fi
fi

