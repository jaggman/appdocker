#!/usr/bin/env bash

service rabbitmq-server start
service php7.1-fpm start
service nginx start
#service cron start
service supervisor start
